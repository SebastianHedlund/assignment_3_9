package com.task.task_3_9;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task39Application {

    public static void main(String[] args) {
        SpringApplication.run(Task39Application.class, args);
    }

}
