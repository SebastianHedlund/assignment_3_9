package com.task.task_3_9.controller;

public class CustomStringMethods {

    public static String reverse(String input){
        char[] charInput = input.toCharArray();

        for (int i = 0; i < input.length() ; i++) {
            charInput[charInput.length -i - 1] = input.charAt(i);
        }

        return String.valueOf(charInput);
    }
}
