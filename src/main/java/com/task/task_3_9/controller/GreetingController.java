package com.task.task_3_9.controller;

import org.springframework.web.bind.annotation.*;

@RestController
public class GreetingController {

    @GetMapping("/")
    public String greeting(){
        return "Hello there! I'm Sebastain.";
    }

    @GetMapping("/greeting/{name}")
    public String greetingUser(@PathVariable String name){
        return "Hello there "+ name + "! I'm Sebastain.";
    }

    @GetMapping("/palindrome")
    public String palindromeChecker(@RequestParam("input") String userInput){
        String reversedUserInput = CustomStringMethods.reverse(userInput);

        if(userInput.equals(reversedUserInput))
            return userInput + " is a palindrome!";

        return userInput + " is not a palindrome...";
    }

}
